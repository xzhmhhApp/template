# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitee.com/xzhmhhApp/template/compare/v0.0.2...v0.0.3) (2022-03-28)

### [0.0.2](https://gitee.com/xzhmhhApp/template/compare/v0.0.1...v0.0.2) (2022-03-28)

### 0.0.1 (2022-03-28)


### Features

* 添加部分依赖 ([e829937](https://gitee.com/xzhmhhApp/template/commit/e82993725ea6c98ae00f4abe28c2fd0cdd2eefc1))
* 添加插件 ([b19f334](https://gitee.com/xzhmhhApp/template/commit/b19f334898d1ac019cbaf1bc328d0bc462e842b9))
* **dependencies:** 添加功能组件 ([e4a0a8d](https://gitee.com/xzhmhhApp/template/commit/e4a0a8d2f95eeffc5cb2d8798efe9fd66bc69509))
* **dependencies:** 新增图片压缩 ([7d8c5bb](https://gitee.com/xzhmhhApp/template/commit/7d8c5bb6be16b448cb174f7d63cad0af436798bb))
* **dependencies:** 修改启动问题 ([bc53a53](https://gitee.com/xzhmhhApp/template/commit/bc53a53db9c425196a13631bbc117db7264176f5))
* **dependencies:** html插件添加 ([07a703c](https://gitee.com/xzhmhhApp/template/commit/07a703c537fb3a03608aca56dd39734803048726))
* **other:** 忽略文件 ([f1a4988](https://gitee.com/xzhmhhApp/template/commit/f1a49884484d2c41eddfe466ed899ef4f6d9f500))
* **other:** 修改commit ([4be4717](https://gitee.com/xzhmhhApp/template/commit/4be471791b19da9da683eeda0737d804f14c52df))
* test ([19220ed](https://gitee.com/xzhmhhApp/template/commit/19220ed9b3ad8ab9ae69f0d78fe0a51ab0d7f8fb))
* **utils:** 修改readme和接口功能 ([59380e4](https://gitee.com/xzhmhhApp/template/commit/59380e41f0b9bb70e7fb0fc201da491d1c0084a3))


### Bug Fixes

* **merge:** 打包报错处理 ([9e0202f](https://gitee.com/xzhmhhApp/template/commit/9e0202f093b6501d803ad800dfd13f667986acc1))
* **merge:** 修改eslit引发的报错及配置 ([6870497](https://gitee.com/xzhmhhApp/template/commit/6870497014fb80ff346f5912b2551a01a08bcce9))
