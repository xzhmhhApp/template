# Vue 3 + Typescript + Vite

This template should help get you started developing with Vue 3 and Typescript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's `.vue` type support plugin by running `Volar: Switch TS Plugin on/off` from VSCode command palette.

## 功能：

#### mock数据 参考 

https://github.com/vbenjs/vite-plugin-mock

#### 代理及其他vite配置

https://cn.vitejs.dev/config/

#### element-plus组件,vue-router,vue方法,Components文件夹组件无需引入可直接使用

[列子](./src/pages/Home/index.vue)

如新增无需导入文件夹，在文件夹配置vite.config.js

```ts
Components({
        resolvers: [ElementPlusResolver()],
        dirs: ['src/components','新增文件夹路径'], // 自动引入路径
        dts: true,
        include: [/\.vue$/, /\.vue\?vue/, /\.md$/]
      }),
```



#### 使用setup语法糖要写name

[列子](./src/components/HelloWorld.vue)
#### 

代码结构

```
└── src/
    ├── assets/             # 静态资源目录
    ├── components/         # 公共组件目录
    ├── pages/              # 页面目录
    	  ├── 页面/            # 页面文件夹
    	  	├── components/   # 页面非公共组件目录（业务组件）
    	  	├── index.vue     # 页面跟文件
    ├── router/             # 路由配置目录
    ├── store/              # 状态管理配置目录
    ├── typings/            # 类型文件目录
    ├── utils/              # 工具方法目录
    ├── App.vue             # 根组件
    ├── env.d.ts            # vue 环境声明，不同版本的 vite 文件可能会有所差异
    ├── main.ts             # 主文件
```



## Installation & Quick start

```js
//1.首先下载依赖
npm install && yarn install
/*
2.下载vscode插件
	eslint
3. npm install -g eslint
4. npm install -g commitizen  可以使用git cz
5. 保存文件时自动修复错误 
	则进入设置页面（文件 -> 首选项 -> 设置），搜索 editor.codeActionsOnSave，打开 settings.json
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
*/
```

## 提交代码

```shell
git add .

git cz && git commit -m 'feat: xxx'

git push 
```

提交规范

- feat：新功能（feature）
- fix：修补bug
- docs：文档（documentation）
- style： 格式（不影响代码运行的变动）
- refactor：重构（即不是新增功能，也不是修改bug的代码变动）
- test：增加测试
- chore：构建过程或辅助工具的变动



## 发布上线版本

发布前先执行 `npm run release` 锁定版本

## 注意事项

1. 如果启动时，没有更新代码或者eslint校验没有更新，删除掉node_modules/.vite,重新启动
2. 陆续更新中。。。

