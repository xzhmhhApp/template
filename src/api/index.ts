import { getRequest } from '@/utils/http/index';

export function getConfig() {
  return getRequest('/aptos/kiko/v1/config');
}
