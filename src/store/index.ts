import { readonly } from 'vue';
import { createAction, createState } from './module/test';

const state = createState();
const action = createAction(state);

export default () => {
  const store = {
    state: readonly(state),
    action: readonly(action)
  };

  return store;
};
