// import { CONSTANTS_STARMASK } from '@/constants/starmask';
import Cookies from 'js-cookie';
import Wallet from '../../wallet/index';

export interface IState {
  token: string;
  stcChainID: string;
  stcProvider: string;
  accounts: string[];
  onboarding: string;
  netChainId: string;
  precision: any; // {stc: 1000000000}
  balances: any; // {tokens: string}
  walletStatus: string; // 钱包链接状态 "unConnected", "connected"
  walletTimer: null;
}

export const State: IState = {
  token: 'STC',
  // stcChainID: CONSTANTS_STARMASK.MAIN_CHIA_ID,
  stcChainID: '0x1',
  stcProvider: '',
  accounts: [],
  onboarding: '',
  netChainId: '',
  precision: {}, // {stc: 1000000000}
  balances: {}, // {tokens: string}
  walletStatus: 'unConnected', // 钱包链接状态 "unConnected", "connected"
  walletTimer: null
};

function updateChainId(state: IState) {
  return (chainId: string) => {
    state.stcChainID = chainId;
  };
}

function updateAccount(state: IState) {
  return (accounts: string[]) => {
    state.accounts = accounts;
  };
}

function updateOnBoarding(state: IState) {
  return (onboarding: string) => {
    state.onboarding = onboarding;
  };
}

function updateProvider(state: IState) {
  return (provider: string) => {
    state.stcProvider = provider;
  };
}

function updateBalance(state: IState) {
  return (balances: any) => {
    state.balances = { ...state.balances, ...balances };
  };
}

function updateConnectStatus(state: IState) {
  return (status: string) => {
    state.walletStatus = status;
    if (status === 'connected') {
      Cookies.set('connect-status', 'connected', { expires: 7 });
    }
  };
}

function updateNetWorkId(state: IState) {
  return (id: string) => {
    state.netChainId = id;
  };
}

function updateWalletStatusTimer(state: IState) {
  return (timer: any) => {
    state.walletTimer = timer;
  };
}

function isConnectWallet(state: IState) {
  return state.walletStatus;
}

/**
 * 创建 Action
 * lijing
 */

export function setStcChianID(state: IState) {
  return {
    updateChainId: updateChainId(state)
  };
}

export function setAccounts(state: IState) {
  return {
    updateAccount: updateAccount(state)
  };
}

export function setOnboarding(state: IState) {
  return {
    updateOnBoarding: updateOnBoarding(state)
  };
}

export function setStcProvider(state: IState) {
  return {
    updateProvider: updateProvider(state)
  };
}

export async function getBalance(state: IState, token: string) {
  const balance = await Wallet.getAccountBalance({
    provider: state.stcProvider,
    account: state.accounts[0],
    token
  });
  return {
    updateBalance: updateBalance(state)
  };
}
