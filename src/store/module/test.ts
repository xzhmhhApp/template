import { reactive } from 'vue';

export interface IState {
  code: string;
  token: string;
  user: any;
}

export const State: IState = {
  code: '',
  token: '',
  user: {}
};

function updateToken(state: IState) {
  return (token: string) => {
    state.token = token;
  };
}

function updateUser(state: IState) {
  return (user: string) => {
    state.user = user;
  };
}

function updateCode(state: IState) {
  return (code: string) => {
    state.code = code;
  };
}
/**
 * 创建Action
 * @param state
 */
export function createAction(state: IState) {
  return {
    updateToken: updateToken(state),
    updateUser: updateUser(state),
    updateCode: updateCode(state)
  };
}
// 创建State
export function createState() {
  return reactive(State);
}
