export class Retry {
  private timer: NodeJS.Timeout | null;

  constructor(
    private cb: () => Promise<any>,
    private time: number,
    private count: number
  ) {
    this.cb = cb;
    this.time = time;
    this.count = count;
    this.timer = null;
  }

  timeout() {
    return new Promise((res, rej) => {
      this.timer = setTimeout(() => {
        if (this.count > 0) {
          console.log('重连', this.count);
          this.count -= 1;
          res(this.returnFn());
        } else {
          rej(new Error('次数用尽'));
          console.log('次数用尽');
        }
      }, this.time);
    });
  }

  returnFn(): Promise<any> {
    return Promise.race([
      this.cb().then(res => {
        clearTimeout(this.timer as NodeJS.Timeout);
        return res;
      }),
      this.timeout()
    ]);
  }
}
