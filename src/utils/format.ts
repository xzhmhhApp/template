// import { computed } from 'vue';
import i18n from '../i18n';
import utilsNumber from './number.js';
import { TypeNumberMetaOption } from './type';

/**
 * 990000000 => 9
 * @param {*} balance
 * @param {*} precision
 * @returns
 */
export const formatBalance = (balance: number, precision: number) => {
  return utilsNumber
    .bigNum(balance)
    .div(10 ** precision)
    .toString();
};

/**
 *
 * @param {*} price
 * @param {*} precision
 * @param {*} options
 * @returns 格式化分组等 展示用 后续使用fly-amount 不使用这个
 */
export const formatPrice = (
  price: number,
  options: TypeNumberMetaOption,
  precision = 9
) => {
  return utilsNumber.formatNumberMeta(
    utilsNumber
      .bigNum(price)
      .div(10 ** precision)
      .toString(),
    {
      trailingZero: false,
      grouped: true,
      ...options
    }
  ).text;
};

// /**
//  * 因为js切换时候刷新会不及时，用computed熟悉
//  * @param {*} content
//  * @returns
//  */
// export const computedLangCtx = (content, options) => {
//   return computed(() => {
//     return i18n.global.t(content, options || {});
//   });
// };
// /**
//  *
//  */

export const formatSliceString = (string: string, backup = '') => {
  if (string) {
    return `${string.slice(0, 6)}...${string.slice(-4)}`;
  }
  return backup;
};
export const computedLangCtx = (content: string, options: any) => {
  return computed(() => {
    return i18n.global.t(content, options || {});
  });
};
