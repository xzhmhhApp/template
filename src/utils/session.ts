export default {
  setItem(k: string, v: string) {
    localStorage.setItem(k, v);
  },
  getItem(k: string) {
    return localStorage.getItem(k) || null;
  },
  removeItem(k: string) {
    localStorage.removeItem(k);
  },
  clear() {
    localStorage.clear();
  }
};
