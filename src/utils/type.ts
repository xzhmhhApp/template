export type TypeNumberMetaOption = {
  precision?: number; // 显示小数位精度，undefined则原始值返回，不进行小数位处理
  round?: number; // 1 => round, 2 => ceil, 3 => floor 小数位处理进位方式
  trailingZero?: Boolean; // 小数位处理，如果小数位不足是否追加 0 (toFixed的默认行为)
  showSign?: Boolean; // 追加正负符号, 根据原始值value大于零判断，而非toFixed之后的数值
  percentStyle?: Boolean; // 百分比形式显示
  compact?: Boolean; // 显示 1.3k 1.5k 格式
  grouped?: Boolean; // 显示 逗号分隔组 1,234,223.183
  nilText?: string; // 任何无效数字的返回值 (如果 nanText 和 infinityText 未赋值，也将使用该值)
  nanText?: string | undefined; // NaN 返回值
  infinityText?: string | undefined; // Infinity 返回值
};
