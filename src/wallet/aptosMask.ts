/*
 * @Descripttion:
 * @version:
 * @Author: xiaziheng
 * @Date: 2022-04-21 15:58:41
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-01 14:38:25
 */
/* eslint-disable */
import { Types, AptosClient } from 'aptos';

/**
 * @name: xiaziheng
 * @msg: 初始化provider
 * @param {*}
 * @return {*}
 */
export const createAptosProvider = async () => {
  let aptosProvider;
  try {
    if (window.aptos) {
      // We must specify the network as 'any' for starcoin to allow network changes
      const aptosProvider = new AptosClient('https://fullnode.devnet.aptoslabs.com/v1');
    }
  } catch (error) {
    console.error(error);
  }
  return aptosProvider;
};

/**
 * @name: xiaziheng
 * @msg: 获取Starmask插件是否安装 下载方法等 实例化
 * @param {*}
 * @return {*}
 */

 /**
  * @name: xiaziheng
  * @msg: 获取sign
  * @param {*}
  * @return {*}
  */


/**
 * @name: xiaziheng
 * @msg: 检测stamask是否安装
 * @param {*}
 * @return {*}
 */
 
/**
 * @name: xiaziheng
 * @msg: 获取下载地址
 * @param {*}
 * @return {*}
 */


/**
 * @name: xiaziheng
 * @msg: 连接钱包
 * @param {*}
 * @return {*}
 */


/**
 * @name: xiaziheng
 * @msg: 断开钱包
 * @param {*}
 * @return {*}
 */

/**
 * @name: xiaziheng
 * @msg: 获取chainid
 * @param {*}
 * @return {*}
 */


 /**
  * @name: xiaziheng
  * @msg: 获取账户
  * @param {*} provider
  * @param {*} account
  * @param {*} token
  * @return {*}
  */

 /**
  * @name: xiaziheng
  * @msg: 获取权限
  * @param {*}
  * @return {*}
  */



 /**
  * @name: xiaziheng
  * @msg: 获取账号权限
  * @param {*}
  * @return {*}
  */





