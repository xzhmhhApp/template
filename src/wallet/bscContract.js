import { ethers } from "ethers";
import { BigNumber } from "@ethersproject/bignumber";
// import { ABI_TOKEN } from "@abi/index";
// import { isUndefined } from "lodash";

/**
 * 读取合约方法
 * @param {*} contractAddress 合约地址
 * @param {*} abi 合约对应的 abi 文件
 * @param {*} funcName 调用的合约方法名
 * @param  {...any} params 传入的参数
 * @returns promise
 */
export function getContract(
  provider,
  contractAddress,
  abi,
  funcName,
  ...params
) {
  const contract = new ethers.Contract(contractAddress, abi, provider);
  console.log(contractAddress, abi, contract, funcName, ...params);
  return new Promise((resolve, reject) => {
    contract[funcName](...params).then(
      (response) => {
        resolve(response);
      },
      (err) => {
        // 合约调用错误
        console.log(err);
        reject(605);
      }
    );
  });
}

/**
 * 写入合约方法
 * @param {*} contractAddress 合约地址
 * @param {*} abi 合约对应的 abi 文件
 * @param {*} funcName 调用的合约方法名
 * @param  {...any} params 传入的参数
 * @returns promise
 */
export function getWriteContract(contractAddress, abi, funcName, ...params) {
  const provider = new ethers.providers.Web3Provider(window.ethereum);
  const contract = new ethers.Contract(contractAddress, abi, provider);
  const contractWithSigner = contract.connect(provider.getSigner());
  return new Promise((resolve, reject) => {
    contractWithSigner[funcName](...params).then(
      (response) => {
        resolve(response);
      },
      (err) => {
        reject(err);
      }
    );
  });
}

/**
 * 读取合约方法（loading）
 * @param {*} contractAddress 合约地址
 * @param {*} abi 合约对应的 abi 文件
 * @param {*} funcName 调用的合约方法名
 * @param  {...any} params 传入的参数
 * @returns promise
 */
export function getContractLoad(contractAddress, abi, funcName, ...params) {
  const provider = new ethers.providers.Web3Provider(window.ethereum);
  const contract = new ethers.Contract(contractAddress, abi, provider);
  return new Promise((resolve, reject) => {
    contract[funcName](...params).then(
      (response) => {
        let timer = setInterval(() => {
          provider
            .getTransactionReceipt(response.hash)
            .then((receipt) => {
              if (receipt) {
                if (receipt.logs.length) {
                  setTimeout(() => {
                    resolve(response);
                  }, 2000);
                } else {
                  // 链上交互失败
                  reject(601);
                }
                clearInterval(timer);
              }
            })
            .catch((err) => {
              // 合约链上交互方法调用错误
              console.log(err);
              reject(604);
            });
        }, 1000);
      },
      (err) => {
        // 合约调用错误
        console.log(err);
        reject(605);
      }
    );
  });
}

/**
 * 写入合约方法 (loading)
 * @param {*} contractAddress 合约地址
 * @param {*} abi 合约对应的 abi 文件
 * @param {*} funcName 调用的合约方法名
 * @param  {...any} params 传入的参数
 * @returns promise
 */
export function getWriteContractLoad(
  provider,
  contractAddress,
  abi,
  funcName,
  cb,
  ...params
) {
  const contract = new ethers.Contract(contractAddress, abi, provider);
  const contractWithSigner = contract.connect(provider.getSigner());
  return new Promise((resolve, reject) => {
    console.log(
      "合约参数：=====",
      contractAddress,
      abi,
      contractWithSigner,
      funcName,
      ...params
    );
    contractWithSigner[funcName](...params).then(
      (response) => {
        cb && cb(response);
        let timer = setInterval(() => {
          provider
            .getTransactionReceipt(response.hash)
            .then((receipt) => {
              if (receipt) {
                if (receipt.logs.length) {
                  setTimeout(() => {
                    resolve(response);
                  }, 2000);
                } else {
                  // 链上交互失败
                  reject(601);
                }
                clearInterval(timer);
              }
            })
            .catch((err) => {
              // 合约链上交互方法调用错误
              console.log(err);
              reject(err);
            });
        }, 1000);
      },
      (err) => {
        reject(err);
      }
    );
  });
}

/**
 * 合约调用预估gasLimit
 * @param {*} contractAddress 合约地址
 * @param {*} abi 合约对应的 abi 文件
 * @param {*} funcName 调用的合约方法名
 * @param  {...any} params 传入的参数
 * @returns promise
 */
export function estimateGas(contractAddress, abi, funcName, ...params) {
  const provider = new ethers.providers.Web3Provider(window.ethereum);
  const contract = new ethers.Contract(contractAddress, abi, provider);
  const contractWithSigner = contract.connect(provider.getSigner());
  return new Promise((resolve, reject) => {
    contractWithSigner["estimateGas"]
      [funcName](...params)
      .then((estimatedGasLimit) => {
        // console.log('接口返回的gasLimit：===', estimatedGasLimit.toString(), '计算后的gasLimit：====', calculateGasMargin(estimatedGasLimit))
        resolve(calculateGasMargin(estimatedGasLimit));
      })
      .catch((err) => {
        reject(err);
      });
  });
}

/**
 * @name: 获取代币信息
 * author: zhannan.zhang
 * @date:2022-04-27
 */
// export async function getCoinInfo(tokenAddress) {
//   // if (isUndefined(accounts) || accounts == 0) return;
//   let symbol = null;
//   let decimal = null;
//   let tokenName = null;
//   // 判断是否是主币
//   let isMainCoin = tokenAddress === ethers.constants.AddressZero;

//   if (isMainCoin) {
//     return {
//       name: "BNB",
//       symbol: "BNB",
//       decimals: 18,
//     };
//   }
//   // 获取代币 symbol
//   await getContract(tokenAddress, ABI_TOKEN, "symbol").then((symbolRes) => {
//     symbol = symbolRes;
//   });
//   // 获取代币 decimals
//   await getContract(tokenAddress, ABI_TOKEN, "decimals").then((decimalRes) => {
//     decimal = decimalRes;
//   });
//   // 获取代币 name
//   await getContract(tokenAddress, ABI_TOKEN, "name").then((nameRes) => {
//     tokenName = nameRes;
//   });
//   // 获取代币当前账户余额
//   // console.log("当前账户", accounts);
//   // await getContract(tokenAddress, ABI_TOKEN, "balanceOf", accounts).then(
//   //   (balanceRes) => {
//   //     console.log(
//   //       "balance:=====",
//   //       ethers.utils.formatUnits(balanceRes, decimal)
//   //     );
//   //   }
//   // );
//   return {
//     decimals: decimal,
//     name: tokenName,
//     symbol: symbol,
//   };
// }

/**
 * 重新计算gasLimit，增加10%
 * @param {*} gasLimit
 */
function calculateGasMargin(value) {
  return value
    .mul(BigNumber.from(10000).add(BigNumber.from(1000)))
    .div(BigNumber.from(10000));
}
