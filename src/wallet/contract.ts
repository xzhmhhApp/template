/*
 * @Descripttion:
 * @version:
 * @Author: xiaziheng
 * @Date: 2022-05-13 10:43:39
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-10 10:42:01
 */
/**
 * 1.相同合约相同方法
 * 2.不同合约相同方法
 * 3.不同合约不同方法
 * 4.abi
 */

import W from "./index.js";

// import * as bsc from "./bscContract.js";

// import * as stc from "./stcContract.js";
import * as martian from "./martianContract";

// import bscInfoClass from "../contract/bscAddress.js";

// import stcInfo from "../contract/stcAddress.js";
import { walletConnectList } from "./config.js";
import { Topic } from "./topic.js";

const contractFn:any = {
  // ...stc,
  ...martian,
};
class Contract {
  connect = "apt"; //连接状态 bsc stc unConnected
  wallet = "Martian";
  constructor() {
    this.connect = W.connect == "unConnected" ? "apt" : W.connect;
    W.on(Topic.changeConnect).subscribe((connect) => {
      // TODO 客户默认
      if (connect == "unConnected") {
        connect = "apt";
      }
      this.connect = connect;
    });
    W.on(Topic.changeWallet).subscribe((wallet) => {
      // TODO 客户默认
      if (!wallet) {
        wallet = "apt";
      }
      this.wallet = wallet;
    });
  }
  /**
   * 读取合约方法 (不需要签名)
   * @param {*} contractName 合约名称
   * //暂无@param {*} abiOrStructTag 合约对应的 abi 文件或者 stc的tag
   * @param {*} funcName 调用的合约方法名
   * @param  {...any} params 传入的参数
   * @returns promise
   */
  getContract(contractName:string, funcName:string, type_arguments:any[], ...params:any) {
    // const bscInfo = bscInfoClass.getAddress();
    // let contractAddress, abiOrStructTag;
    if (
      walletConnectList[this.connect] &&
      walletConnectList[this.connect].indexOf(this.wallet) !== -1
    ) {
      if (contractFn[`get${this.wallet}GenerateContract`]) {
        return contractFn[`get${this.wallet}GenerateContract`](
          contractName,
          funcName,
          type_arguments,
          ...params
        );
      }
    }
  }
  /**
   * @name: xiaziheng
   * @msg:
   * @param {*} contractName 合约名称
   * //暂无@param {*} abi abi
   * @param {*} funcName 调用的合约方法名
   * @param {*} tyArgs  合约需要的辨识参数（metaData, metaBody），没有传[]
   * @param {*} cb  合约调用成功回调，还未上链成功
   * @param {array} params 参数
   * @return {*}
   */
  setContract(contractName:string, funcName:string, tyArgs:any[], cb:()=>void, ...params:any) {
    // const bscInfo = bscInfoClass.getAddress();
    // let contractAddress, abi;

    if (
      walletConnectList[this.connect] &&
      walletConnectList[this.connect].indexOf(this.wallet) !== -1
    ) {
      if (contractFn[`get${this.wallet}GenerateContract`]) {
        return contractFn[`get${this.wallet}ContractWithSinger`](
          W.provider,
          contractName,
          funcName,
          tyArgs,
          cb,
          ...params
        );
      }
    }
  }
  /**
   * @name: xiaziheng
   * @msg:
   */
  getNftID(hash:string) {
    // const bscInfo = bscInfoClass.getAddress();
    // let contractAddress, abi;
    if (
      walletConnectList[this.connect] &&
      walletConnectList[this.connect].indexOf(this.wallet) !== -1
    ) {
      if (contractFn[`get${this.wallet}NFTID`]) {
        return contractFn[`get${this.wallet}NFTID`](hash);
      }
    }
  }
 

  static init() {
    return new Contract();
  }
}

export default Contract.init();
