/*
 * @Descripttion:钱包公共方法
 * @version:
 * @Author: xiaziheng
 * @Date: 2022-04-19 16:25:31
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-14 20:03:50
 */
import Cookies from 'js-cookie';
import { Emiter } from '../utils/Emiter';
// import utilsNumber from "@utils/number.js";
// import {
//   checkMetaMaskInstalled,
//   getMetaMaskInstallUrl,
//   sendRequest
// } from './metaMask';
// import * as starMask from './starMask';
// import { getStarMaskInstallUrl, checkStarMaskInstalled } from './starMask';
import * as martianMask from './martianMask';
import { connectList, walletConnectList } from './config';
import { Topic } from './topic';
import { AllocChainidType } from './type';

// import CONSTANTS_STARMASK from "@constants/starmask.js";

const walletFn: any = {
  // ...starMask,
  ...martianMask
};
const allocChainid: AllocChainidType = {
  apt: ['1', '20', '35'],
  stc: ['0x1']
};
class Wallet extends Emiter<Topic> {
  connect = 'unConnected'; // 连接状态 bsc stc unConnected

  wallet: string | null = null;

  accounts: string | null = null;

  provider: any = {};

  chainid: string | null = null;

  balance: number = 0;

  constructor() {
    super();
    this.walletInit();
    this.balanceChange();
    this.accountMetaChanged();
  }

  /**
   * @name: xiaziheng
   * @msg: 初始化
   * @param {*}
   * @return {*}
   */
  async walletInit(type?: string) {
    if (type && connectList.indexOf(type) !== -1) {
      this.connect = type;
    } else {
      this.connect = (
        !Cookies.get('connect-status-new') ||
        Cookies.get('connect-status-new') === 'unConnected'
          ? 'unConnected'
          : Cookies.get('connect-status-new')
      )!;
    }
    const walletType = Cookies.get('walletType') || null;
    if (
      walletConnectList[this.connect] &&
      walletConnectList[this.connect].indexOf(walletType) !== -1
    ) {
      this.wallet = walletType;
    }
    if (this.connect !== 'unConnected' && this.wallet) {
      await this.connectMask(this.connect, this.wallet);
    }
    await this.createProvider(this.connect);
    this.balanceChangeFn(this.connect, true);
    // this.getNetwork();
  }

  /**
   * @name: xiaziheng done
   * @msg: 创建provider
   * @param {*}
   * @return {*}
   */
  async createProvider(connect: string) {
    if (
      walletConnectList[connect] &&
      walletConnectList[connect].indexOf(this.wallet) !== -1
    ) {
      if (walletFn[`createStar${this.wallet}Provider`]) {
        this.provider = await walletFn[`createStar${this.wallet}Provider`]();
      }
    }
  }

  /**
   * @name: xiaziheng done
   * @msg: 网络切换监听
   * @param {*}
   * @return {*}
   */
  changeConnect() {
    Cookies.set('connect-status-new', this.connect, { expires: 7 });
    this.emit(Topic.changeConnect, this.connect);
  }

  changeWallet() {
    if (this.wallet) {
      Cookies.set('walletType', this.wallet, { expires: 7 });
    } else {
      Cookies.remove('walletType');
    }

    this.emit(Topic.changeWallet, this.wallet);
  }

  /**
   * @name: xiaziheng
   * @msg: 账户断开
   * @param {*}
   * @return {*}
   */
  unconnectMask() {
    this.connect = 'unConnected';
    this.wallet = null;
    this.changeConnect();
    this.changeWallet();
  }

  /**
   * @name: xiaziheng done
   * @msg: 检测是否有钱包
   * @param {*}
   * @return {*}
   */
  // checkIsInstalled() {
  //   // switch (this.connect) {
  //   //   case 'stc':
  //   //     return checkStarMaskInstalled();
  //   //   default:
  //   //     return checkMetaMaskInstalled();
  //   // }
  // }

  /**
   * @name: xiaziheng done
   * @msg: 钱包下载方法
   * @param {*}
   * @return {*}
   */
  // installMask() {
  //   // switch (this.connect) {
  //   //   case 'bsc':
  //   //     return getMetaMaskInstallUrl();
  //   //   default:
  //   //     return getStarMaskInstallUrl();
  //   // }
  // }

  /**
   * @name: xiaziheng
   * @msg: 钱包连接方法 done
   * @param {*}
   * @return {*}
   */
  async connectMask(connect: string, wallet: string) {
    let accounts;
    try {
      if (
        walletConnectList[connect] &&
        walletConnectList[connect].indexOf(wallet) !== -1
      ) {
        if (walletFn[`connect${wallet}Mask`]) {
          accounts = await walletFn[`connect${wallet}Mask`]();
        }
      }
      if (connect !== this.connect) {
        this.connect = connect;
        this.changeConnect();
      }
      if (wallet !== this.wallet) {
        this.wallet = wallet;
        this.changeWallet();
      }
    } catch (error) {
      this.connect = 'unConnected';
      accounts = [];
      // this.changeConnect();
      throw error;
    }
    if (this.connect !== 'unConnected') {
      this.accounts = accounts;
    }
    return accounts;
  }

  getCurrentAccount() {
    return this.accounts;
  }

  /**
   * @name: xiaziheng
   * @msg: 获取chainid
   * @param {*}
   * @return {*}
   */
  async getNetwork() {
    let open = false;
    try {
      if (
        walletConnectList[this.connect] &&
        walletConnectList[this.connect].indexOf(this.wallet) !== -1
      ) {
        if (walletFn[`connect${this.wallet}Mask`]) {
          this.chainid = await walletFn[`get${this.wallet}NetworkChainId`]();
        }
      }
      if (this.chainid)
        open =
          allocChainid[this.connect] &&
          allocChainid[this.connect].indexOf(this.chainid) !== -1;
    } catch (error) {
      console.error(error);
    }
    return open;
  }

  /**
   * @name: xiaziheng
   * @msg: 获取网络id
   * @param {*}
   * @return {*}
   */
  getNetworkID() {
    return this.chainid;
  }

  /**
   * @name: xiaziheng
   * @msg: 事件监听
   * @param {*}
   * @return {*}
   */
  accountMetaChanged() {
    if (!window.ethereum) return;
    // 账户切换
    window.ethereum.on('accountsChanged', (accounts: string) => {
      if (this.connect === 'bsc') {
        if (accounts.length === 0) {
          this.connect = 'unConnected';
        } else {
          this.accounts = accounts;
        }
        this.emit(Topic.accountsChanged, accounts);
      }
    });
    // 网络切换
    window.ethereum.on('chainChanged', (id: number) => {
      if (this.connect === 'bsc') {
        this.chainid = id.toString();
        this.emit(Topic.chainChanged, id);
      }
    });
    window.starcoin.on('accountsChanged', (accounts: string) => {
      if (this.connect === 'stc') {
        if (accounts.length === 0) {
          this.connect = 'unConnected';
        } else {
          this.accounts = accounts;
        }
        this.emit(Topic.accountsChanged, accounts);
      }
    });
    window.starcoin.on('chainChanged', (id: number) => {
      if (this.connect === 'stc') {
        this.chainid = String(id);
        this.emit(Topic.chainChanged, id);
      }
    });
    // 账户切换
    window.martian.onAccountChange((accounts: string) => {
      console.log(accounts);
      if (!accounts) {
        this.connect = 'unConnected';
      } else {
        this.accounts = accounts;
      }
      this.emit(Topic.accountsChanged, accounts);
    });
    // 网络切换
    window.martian.onNetworkChange((netName: string) => {
      console.log(netName);
      this.emit(Topic.chainChanged, netName);
    });
  }

  async getBalance() {
    let balance;
    try {
      if (!this.accounts) {
        return 0;
      }
      if (
        walletConnectList[this.connect] &&
        walletConnectList[this.connect].indexOf(this.wallet) !== -1
      ) {
        if (walletFn[`connect${this.wallet}Mask`]) {
          balance = await walletFn[`get${this.wallet}AccountBalance`]();
        }
      }
      if (this.connect !== 'unConnected') {
        this.balance = balance;
      }
      return balance;
    } catch (error) {
      if (this.connect !== 'unConnected') {
        this.balance = 0;
      }
      console.log(error);
      return '0';
    }
  }

  /**
   * @name: xiaziheng
   * @msg: 余额监听事件
   * @param {*}
   * @return {*}
   */
  balanceChange() {
    const oldConnect = this.connect;
    setInterval(async () => {
      this.balanceChangeFn(oldConnect);
    }, 5000);
  }

  async balanceChangeFn(oldConnect: string, open?: boolean) {
    if (!this.connect) return;

    const oldBalance = this.balance || 0;

    const newBalance = await this.getBalance();
    if (oldConnect === this.connect && oldBalance !== newBalance) {
      this.emit(Topic.balanceChange, newBalance);
    }
    if (open) {
      this.emit(Topic.balanceChange, newBalance);
    }
  }

  // sendRequest(param: any) {
  //   return sendRequest(this.provider, param);
  // }

  static init() {
    return new Wallet();
  }
}

export default Wallet.init();
