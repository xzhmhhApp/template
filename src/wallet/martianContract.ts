// import request from "@utils/request";
// import { AptosClient } from "aptos";

import { GetContractWithSingerType, GetGenerateContract } from './type';

/**
 * @name: 调用合约方法 (需要签名)
 * @providers: provider
 * @contract: 合约地址
 * @funcName: 调用的合约方法
 * @tyArgs: 合约需要的辨识参数（metaData, metaBody），没有传[]
 * @...args: 入参
 * author: zhannan.zhang
 * @date:2022-05-11
 */
const aptAddress = JSON.parse(localStorage.getItem('config')!).supportContract;
export const getMartianContractWithSinger: GetContractWithSingerType = async (
  providers,
  contractName,
  funcName,
  tyArgs,
  cb,
  ...args
) => {
  try {
    // Create a transaction
    const response = await window.martian.connect();
    const sender = response.address;
    const payload = {
      function: `${aptAddress[contractName]}::${funcName}`,
      type_arguments: tyArgs,
      arguments: args
    };
    console.log(payload, '合约调取');
    const transaction = await window.martian.generateTransaction(
      sender,
      payload
    );
    const txnHash = await window.martian.signAndSubmitTransaction(transaction);

    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    cb && cb({ hash: txnHash });
    return await new Promise(resolve => {
      const timer = setInterval(() => {
        window.martian.getTransaction(txnHash).then((res: any) => {
          console.log(res);
          if (res.success) {
            resolve(200);
            clearInterval(timer);
          } else {
            resolve('failed');
            console.error('合约失败:', res.result);
          }
        });
      }, 1000);
    });
  } catch (error) {
    console.log('contract err', error);
    throw error;
  }
};

/**
 * @name: xiaziheng
 * @msg: 调用合约方法 (不需要签名)
 * @param {*} functionId 方法名
 * @param {*} type_arguments type
 * @param {array} params 请求参数
 * @return {*}
 */
export const getMartianGenerateContract: GetGenerateContract = async (
  contractName,
  funcName,
  type_arguments,
  ...params
) => {
  const response = await window.martian.connect();
  const sender = response.address;
  const payload = {
    function: `${aptAddress[contractName]}::${funcName}`,
    type_arguments,
    arguments: params
  };
  // example to set custom gas amount, default options are mentioned above
  const options = {};
  const transactionRequest = await window.martian.generateTransaction(
    sender,
    payload,
    options
  );
  return transactionRequest;
};

/**
 * @name: xiaziheng
 * @msg: 查询连上信息
 * @param {*} functionId 方法名
 * @param {*} type_arguments type
 * @param {array} params 请求参数
 * @return {*}
 */
export async function getMartianTransactionInfo(hash: string) {
  // Fetch transaction details
  const res = await window.martian.getTransaction(hash);
  return res;
}
/**
 * @name: xiaziheng
 * @msg: 查询nftId
 * @param {*} functionId 方法名
 * @param {*} type_arguments type
 * @param {array} params 请求参数
 * @return {*}
 */
export async function getMartianNFTID(hash: string) {
  // Fetch transaction details
  const info = await getMartianTransactionInfo(hash);
  console.log(info);
  const idInfo = info.events.filter((item: any) => {
    return item.type === `${aptAddress.kiko_owner}::open_box::OpenBoxEvent`;
  })[0];
  return `${idInfo.data.token_id.token_data_id.creator}@${idInfo.data.token_id.token_data_id.collection}@${idInfo.data.token_id.token_data_id.name}@${idInfo.data.token_id.property_version}`;
}
/**
 * @name: xiaziheng
 * @msg: 查询nftId
 * @return {*}
 */
export function getMartianTokenID2Other(tokenId: string) {
  // Fetch transaction details
  const arr = tokenId.split('@');
  if (arr.length === 4) {
    return {
      creator: arr[0],
      collection: arr[1],
      name: arr[2],
      property_version: arr[3]
    };
  }
  return {};
}
