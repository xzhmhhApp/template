/*
 * @Descripttion:
 * @version:
 * @Author: xiaziheng
 * @Date: 2022-04-21 15:58:41
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-01 14:38:25
 */
import { formatPrice } from '@/utils/format';
import { AptosClient } from 'aptos';
/**
 * @name: xiaziheng
 * @msg: 初始化provider
 * @param {*}
 * @return {*}
 */
export const createAptosProvider = async () => {
  let aptosProvider;
  try {
    if (window.martian) {
      // We must specify the network as 'any' for starcoin to allow network changes
      aptosProvider = new AptosClient(
        'https://fullnode.devnet.aptoslabs.com/v1'
      );
    }
  } catch (error) {
    console.error(error);
  }
  return aptosProvider;
};

/**
 * @name: xiaziheng
 * @msg: 获取mask插件是否安装 下载方法等 实例化
 * @param {*}
 * @return {*}
 */

export const getProvider = () => {
  if ('martian' in window) {
    return window.martian;
  }
  window.open('https://www.martianwallet.xyz/', '_blank');
  return '';
};
/**
 * @name: xiaziheng
 * @msg: 获取sign
 * @param {*}
 * @return {*}
 */
export const getSign = async (message: string) => {
  try {
    const metadata = {
      address: true,
      application: true,
      chainId: true,
      message,
      nonce: 12345
    };
    const signature = await window.martian.signMessage(metadata);
    console.log(signature);

    return signature;
  } catch (err) {
    console.error(err);
    return '';
  }
};

/**
 * @name: xiaziheng
 * @msg: 检测mask是否安装
 * @param {*}
 * @return {*}
 */
export const checkMartianMaskInstalled = () => {
  return getProvider();
};

/**
 * @name: xiaziheng
 * @msg: 获取下载地址
 * @param {*}
 * @return {*}
 */
export const getMartianMaskInstallUrl = () => {
  return 'https://www.martianwallet.xyz/';
};

/**
 * @name: xiaziheng
 * @msg: 连接钱包
 * @param {*}
 * @return {*}
 */

export const connectMartianMask = async () => {
  let newAccounts;
  try {
    newAccounts = await window.martian.connect();
    console.log(newAccounts);
  } catch (error) {
    // eslint-disable-next-line @typescript-eslint/no-throw-literal
    throw {
      type: 'aptos',
      url: 'https://www.martianwallet.xyz/',
      error
    };
  }
  return newAccounts.address;
};
/**
 * @name: xiaziheng
 * @msg: 断开钱包
 * @param {*}
 * @return {*}
 */

export const disconnectConnectMask = async () => {
  try {
    await window.martian.disconnect();
  } catch (error) {
    // eslint-disable-next-line @typescript-eslint/no-throw-literal
    throw {
      type: 'aptos',
      url: 'https://www.martianwallet.xyz/',
      error
    };
  }
};

/**
 * @name: xiaziheng
 * @msg: 获取chainid
 * @param {*}
 * @return {*}
 */
export const getMartianNetworkChainId = async () => {
  const chainInfo = await window.martian.getChainId();
  return chainInfo.chainId;
};

/**
 * @name: xiaziheng
 * @msg: 获取账户
 * @param {*} provider
 * @param {*} account
 * @param {*} token
 * @return {*}
 */
export const getMartianAccountBalance = async () => {
  try {
    const client = new AptosClient('https://fullnode.devnet.aptoslabs.com');
    const response = await window.martian.connect();
    const { address } = response;
    const resource: any = await client.getAccountResource(
      address,
      '0x1::coin::CoinStore<0x1::aptos_coin::AptosCoin>'
    );
    // const response = await window.martian.connect();
    // const address = response.address;
    // const res = await getAptBalance(address,'0x1::aptos_coin::AptosCoin');
    if (resource == null) {
      return null;
    }
    return formatPrice(parseInt(resource.data.coin.value as string, 10), {}, 8);
  } catch (error) {
    console.error(error);
    return '0';
  }
};

/**
 * @name: xiaziheng
 * @msg: 获取当前账户
 * @param {*} provider
 * @param {*} account
 * @param {*} token
 * @return {*}
 */
export const getMartianAccount = async () => {
  try {
    const response = await window.martian.connect();
    const { address } = response;
    return address;
  } catch (error) {
    console.error(error);
    return '';
  }
};
