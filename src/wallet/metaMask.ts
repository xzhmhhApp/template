/*
 * @Descripttion:
 * @version:
 * @Author: xiaziheng
 * @Date: 2022-04-21 15:29:22
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-14 15:21:27
 */
import { ethers } from 'ethers';
/**
 * @name: xiaziheng
 * @msg: bscProvider创建
 * @param {*}
 * @return {*}
 */
type CreateBscProviderType = () => Promise<
  ethers.providers.Web3Provider | undefined
>;
export const createBscProvider: CreateBscProviderType = async () => {
  let provider;
  if (!window.ethereum) {
    console.log('download ethereum');
    return undefined;
  }
  try {
    console.log(ethers);
    provider = await new ethers.providers.Web3Provider(window.ethereum);
  } catch (error) {
    console.error(error);
  }
  return provider;
};

/**
 * @name: xiaziheng
 * @msg: 检测网络
 * @param {*}
 * @return {*}
 */

export const checkBscNetwork = (provider: ethers.providers.Web3Provider) => {
  return new Promise((res, rej) => {
    if (window.ethereum) {
      provider
        .getNetwork()
        .then(Network => {
          res(Network);
        })
        .catch(err => {
          rej(err);
        });
    } else {
      rej(new Error(''));
    }
  });
};

/**
 * @name: xiaziheng
 * @msg: 检测是否有钱包
 * @param {*}
 * @return {*}
 */
export const checkMetaMaskInstalled = () => {
  return window.ethereum && window.ethereum.isMetaMask;
};
/**
 * @name: xiaziheng
 * @msg: 封装请求
 * @param {*} param send 参数
 * @return {*}
 */
export const sendRequest = (provider: any, param: any): Promise<any> => {
  return new Promise((reslove, reject) => {
    // let provider = new ethers.providers.Web3Provider(window.ethereum);
    provider.provider.sendAsync(param, (err: any, res: any) => {
      let e: Error = new Error(``);
      if (!err && res.error) {
        e = new Error(`EthQuery - RPC Error - ${res.error.message}`);
      }

      if (e) reject(e);
      reslove(res);
    });
  });
};
/**
 * @name: xiaziheng
 * @msg: 获取账户
 * @param {*}
 * @return {*}
 */
export const initAccount = async (provider: ethers.providers.Web3Provider) => {
  const param = {
    method: 'eth_accounts'
  };
  const res: any = await sendRequest(provider as any, param);
  if (res.result.length === 0) {
    return [null, res.result];
  }
  return ['bsc', res.result];
};

/**
 * @name: xiaziheng
 * @msg: 连接钱包 public
 * @param {*}
 * @return {*}
 */
export const connectMetaMask = () => {
  return new Promise((res, rej) => {
    if (window.ethereum) {
      window.ethereum
        .request({ method: 'eth_requestAccounts' })
        .then((accounts: string[]) => {
          if (accounts.length === 0) {
            rej(new Error('Please connect to a wallet.'));
          } else {
            res(accounts);
          }
        })
        .catch((err: any) => {
          if (err.code === 4001) {
            rej(new Error('Please connect to a wallet.'));
          } else {
            rej(
              new Error(`{
                type: 'MetaMask',
                url: ${`https://metamask.app.link/dapp/${window.location.host}?isConnect=bsc`},
                error: 'no wallet'
              }`)
            );
          }
        });
    } else {
      rej(
        new Error(`{
        type: 'MetaMask',
        url: ${`https://metamask.app.link/dapp/${window.location.host}?isConnect=bsc`},
        error: 'no wallet'
      }`)
      );
      // window.open("https://metamask.io/", "_self");
    }
  });
};

/**
 * @name: xiaziheng
 * @msg: 更新余额
 * @param {*}
 * @return {*}
 */
// TODO 扩展 其他erc20代币查询
export const getMetaBalance = async ({ provider, accounts }: any) => {
  const param = {
    params: [accounts],
    method: 'eth_getBalance'
  };
  const res: any = await sendRequest(provider, param);
  if (res.result) {
    const balance = parseInt(res.result, 16) / 1e18;
    return balance;
  }
  return null;
};

/**
 * @name: xiaziheng
 * @msg: 获取下载地址
 * @param {*}
 * @return {*}
 */
export const getMetaMaskInstallUrl = () => {
  return 'https://metamask.io/';
};
