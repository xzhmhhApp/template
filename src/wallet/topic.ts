export enum Topic {
  changeConnect,
  changeWallet,
  accountsChanged,
  chainChanged,
  balanceChange
}
