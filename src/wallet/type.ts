export type AllocChainidType = {
  [key: string]: string[];
};
export type GetAccountType = { provider: any; accounts: string; token: string };
export type GetContractWithSingerType = (
  providers: any,
  contractName: string,
  funcName: string,
  tyArgs: any[],
  cb: (opyion: any) => void,
  ...args: any
) => Promise<any>;

export type GetGenerateContract = (
  contractName: string,
  funcName: string,
  type_arguments: any[],
  ...params: any
) => Promise<any>;
