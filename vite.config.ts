/* eslint-disable import/no-extraneous-dependencies */
import { ConfigEnv, UserConfigExport, loadEnv } from 'vite';
import vue from '@vitejs/plugin-vue';
import legacyPlugin from '@vitejs/plugin-legacy';
// import usePluginImport from 'vite-plugin-importer';
import ViteRestart from 'vite-plugin-restart';

import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

import { resolve } from 'path';
import { viteMockServe } from 'vite-plugin-mock';
import vueJsx from '@vitejs/plugin-vue-jsx';
import autoImport from 'unplugin-auto-import/vite';

import eslintPlugin from 'vite-plugin-eslint';
import VueSetupExtend from 'vite-plugin-vue-setup-extend';
import html from './vite.config/html';
import createCompression from './vite.config/compression';
import createVitePlugins from './vite.config/imageMin';
// https://vitejs.dev/config/
export default ({ mode, command }: ConfigEnv): UserConfigExport => {
  const env = loadEnv(mode, process.cwd());
  return {
    base: './', // 打包路径
    plugins: [
      legacyPlugin({
        targets: [
          'Android > 39',
          'Chrome >= 60',
          'Safari >= 10.1',
          'iOS >= 10.3',
          'Firefox >= 54',
          'Edge >= 15'
        ]
      }),

      ViteRestart({
        restart: ['my.config.[jt]s']
      }),
      Components({
        resolvers: [ElementPlusResolver()],
        dirs: ['src/components', 'src/KVerseUI'],
        dts: true,
        include: [/\.vue$/, /\.vue\?vue/, /\.md$/]
      }),
      autoImport({
        imports: ['vue', 'vue-router'],
        dts: './src/auto-import.d.ts'
      }),
      vue(),
      vueJsx(),
      ...createCompression(env),
      eslintPlugin(),
      html(env, command === 'build'),
      createVitePlugins(env, command === 'build'),
      VueSetupExtend(),
      viteMockServe({
        // default
        mockPath: 'mock',
        localEnabled: command === 'serve'
      })
    ],
    resolve: {
      // 配置 @ 映射到 src 目录
      alias: {
        '@': resolve(__dirname, 'src')
      }
    },
    server: {
      port: 3000, // 端口
      open: true, // 启动打开浏览器
      cors: true, // 跨域
      proxy: {
        '/aptos': {
          target: 'http://52.77.131.111:8890/', // 目标地址
          changeOrigin: true, // 修改源
          secure: false // ssl
          // rewrite: path => path.replace('/aptos/', '/')
        }
      }
    }
  };
};
