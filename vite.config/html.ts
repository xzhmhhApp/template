// eslint-disable-next-line import/no-extraneous-dependencies
import { createHtmlPlugin } from 'vite-plugin-html';

export default function createHtml(
  env: Record<string, string>,
  isBuild: boolean = false
) {
  const { VITE_APP_TITLE, VITE_APP_DEBUG_TOOL } = env;
  const html = createHtmlPlugin({
    inject: {
      data: {
        title: VITE_APP_TITLE,
        debugTool: VITE_APP_DEBUG_TOOL,
        copyrightScript: `
<script>

</script>
                `
      }
    },
    minify: isBuild
  });
  return html;
}
