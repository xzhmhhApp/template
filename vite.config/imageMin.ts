// 图片压缩
// https://github.com/anncwb/vite-plugin-imagemin
// eslint-disable-next-line import/no-extraneous-dependencies
import viteImagemin from 'vite-plugin-imagemin';

function configImageminPlugin() {
  const plugin = viteImagemin({
    gifsicle: {
      optimizationLevel: 7,
      interlaced: false
    },
    optipng: {
      optimizationLevel: 7
    },
    webp: {
      quality: 75
    },
    mozjpeg: {
      quality: 8
    },
    pngquant: {
      quality: [0.8, 0.9],
      speed: 4
    },
    svgo: {
      plugins: [
        {
          removeViewBox: false
        },
        {
          removeEmptyAttrs: false
        }
      ]
    }
  });
  return plugin;
}

export default (viteEnv: Record<string, string>, isBuild: boolean = false) => {
  // ...
  if (isBuild) {
    // vite-plugin-imagemin
    return configImageminPlugin();
  }

  return null;
};
